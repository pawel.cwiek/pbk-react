import React, {Component, PropTypes} from 'react'

export default class Rating extends Component {

    constructor(props) {
        super(props)
        this.state = {
            valueToShow: props.valueToShow,
            stars: this.getStars(props.valueToShow)
        }
    }

    getRate() {
        return Math.round(this.state.valueToShow);
    }

    getStars(activeCount) {
        if (typeof activeCount === 'undefined') {
            activeCount = this.getRate()
        }
        let stars = []
        for (let i = 0; i < this.props.count; i++) {
            stars.push({
                active: i <= activeCount - 1
            })
        }
        return stars
    }

    mouseOver(event) {
        console.log("[Rating] mouseOver")
        if (!this.props.edit) return;
        let index = Number(event.target.getAttribute('data-index'))
        //index okresla ile jest zaznaczonyc strzalek
        index = index + 1
        this.setState({
            stars: this.getStars(index)
        })
    }

    mouseLeave() {
        if (!this.props.edit) return
        this.setState({
            stars: this.getStars()
        })
    }

    clicked(event) {
        if (!this.props.edit) return
        let index = Number(event.target.getAttribute('data-index'))
        let value = index + 1
        this.setState({
            valueToShow: value,
            stars: this.getStars(index)
        })
        this.props.onChange(value)
    }

    render() {
        return (
            <div>
                {
                    this.state.stars.map((star, i) => {
                        return (
                            <span
                                className={star.active ? "Rating__selected" : "Rating"}
                                key={i}
                                data-index={i}
                                onMouseOver={this.mouseOver.bind(this)}
                                onMouseMove={this.mouseOver.bind(this)}
                                onMouseLeave={this.mouseLeave.bind(this)}
                                onClick={this.clicked.bind(this)}>
                    {this.props.char}
                    </span>
                        )
                    })
                }
            </div>
        )
    }
}

Rating.propTypes = {
    edit: PropTypes.bool,
    valueToShow: PropTypes.number,
    count: PropTypes.number,
    char: PropTypes.string,
    size: PropTypes.number,
}

Rating.defaultProps = {
    edit: true,
    valueToShow: 0,
    count: 5,
    char: "★",
    size: 15,
}