import React, {Component, PropTypes} from 'react'
export default class Pagination extends Component {

    onPageLinkClick(event) {
        this.props.onPageLinkClick(Number(event.target.innerHTML));
    }

    render() {
        let pages = Math.ceil(this.props.total / this.props.pageSize);
        let pageItems = [];

        for (let i = 0; i < pages; i++) {
            pageItems.push(
                i + 1
            );
        }
        return (
            <div className="Pagination">
                {this.props.page != 1 && <div className="Pagination-button">
                    <button
                        onClick={this.props.onPreviousPageLinkClick}>
                        previous
                    </button>
                </div>}
                {pageItems.map((pageItem) => (
                    <div key={pageItem} onClick={this.onPageLinkClick.bind(this)}
                         className="Pagination-button">{pageItem}</div>
                ))}
                {this.props.page != pages && <div className="Pagination-button">
                    <button
                        onClick={this.props.onNextPageLinkClick}>
                        previous
                    </button>
                </div>}
            </div>
        )
    }
}

Pagination.propTypes = {
    page: PropTypes.number,
    total: PropTypes.number,
    pageSize: PropTypes.number,
    onPreviousPageLinkClick: PropTypes.func,
    onNextPageLinkClick: PropTypes.func,
    onPageLinkClick: PropTypes.func
}