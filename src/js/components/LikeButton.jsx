import React, {Component, PropTypes} from 'react'
import Icon, {ICONS} from './Icon.jsx';
export default class LikeButton extends React.Component {

    constructor(props) {
        super(props);
        this.state = {likeCount: this.props.likeCount, cssClass: (this.props.count > 1) ? "LikeButton__selected" :"LikeButton" }
    }

    onClickHandler(event) {
        this.setState({
            likeCount: this.state.likeCount + 1
        });
    }

    render() {
        return (
            <div className={this.state.likeCount > 0 ? 'LikeButton__selected' : 'LikeButton'} onClick={this.onClickHandler.bind(this)}>
                <Icon icon={ICONS.HEART} size="20"/>
            </div>
        )
    }
}

LikeButton.propTypes = {
    likeCount: PropTypes.number,
    cssClass: PropTypes.string,
}
LikeButton.defaultProps = {
    likeCount: 0,
    cssClass: 'LikeButton'
}