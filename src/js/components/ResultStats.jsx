import React, {Component, PropTypes} from 'react'

export default class ResultStats extends React.Component {
    render() {
        return (
            <div className="ResultStats">
                <div className="ResultStats__info">{this.props.label}</div>
                <div className="ResultStats__result">{this.props.value}</div>
            </div>
        );
    }
}

ResultStats.propTypes = {
    value: PropTypes.number,
    label: PropTypes.string
}