import React, {Component, PropTypes} from 'react'
import CheckboxFilterPanelItem from './CheckboxFilterPanelItem.jsx'
export default class CheckboxFilterPanel extends Component {

    // TODO[css napisać od nowa]
    constructor(props) {
        super(props)
        this.handleSchowMoreClick = this.handleSchowMoreClick.bind(this);
        this.handleHideMoreClick = this.handleHideMoreClick.bind(this);
        if (this.props.data.length > this.props.maxItemsShow) {
            // calculateed the sum of the remaining count
            var remainedCount = this.props.data.slice(this.props.maxItemsShow).reduce(function (previousValue, item) {
                return previousValue + item.count;
            }, 0);

            this.state = {
                remainedCount: remainedCount,
                items: this.props.data.slice(0, this.props.maxItemsShow),
                showMoreVisible: true,
                isShowMore: false
            }
        }
        else {
            this.state = {
                items: this.props.data,
                showMoreVisible: false
            }
        }
    }

    handleSchowMoreClick() {
        this.setState({
            isShowMore: true,
            items: this.props.data
        });
    }

    handleHideMoreClick() {
        this.setState({
            isShowMore: false,
            items: this.props.data.slice(0, this.props.maxItemsShow)
        });
    }


    render() {
        let button = null;
        if (this.state.isShowMore) {
            button = <p className="CheckboxFilterPanel__p">
                <a className="CheckboxFilterPanel__a" href="#" onClick={this.handleHideMoreClick}>
                    - Hide more...
                </a>
            </p>
        } else {
            button = <div>
                <p className="CheckboxFilterPanel__p CheckboxFilterPanel__display">
                    <a className="CheckboxFilterPanel__a" href="#" onClick={this.handleSchowMoreClick}>
                        + Schow more...
                    </a>
                </p>
                <p className="CheckboxFilterPanel__p CheckboxFilterPanel__display">{this.state.remainedCount}</p>
            </div>
        }

        return (
            <div className="CheckboxFilterPanel">
                <div className="CheckboxFilterPanel__panel-title CheckboxFilterPanel__display">{this.props.label}</div>
                <div className="CheckboxFilterPanel__panel-close CheckboxFilterPanel__display">x</div>
                {
                    this.state.items.map(item =>
                        <CheckboxFilterPanelItem key={item.id} data={item}
                                                 onCheckBoxFilterChange={this.props.onCheckBoxFilterChange}/>
                    )
                }
                {
                    this.state.showMoreVisible &&
                    button
                }
            </div>
        )
    }
}

CheckboxFilterPanel.propTypes = {
    label: PropTypes.string,
    data: PropTypes.array,
    maxItemsShow: PropTypes.number,
    onCheckBoxFilterChange: PropTypes.func
}

CheckboxFilterPanel.defaultProps = {
    label: 'Title',
    data: [],
    maxItemsShow: 7
}