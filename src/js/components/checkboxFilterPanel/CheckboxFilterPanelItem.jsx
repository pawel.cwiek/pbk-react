import React, {Component, PropTypes} from 'react'
export default class CheckboxFilterPanelItem extends React.Component {

    render() {
        var labelStyle = {
            'textDecoration': this.props.data.selected ? 'line-through' : ''
        };

        return (
            <div>
                <label style={labelStyle} className="CheckboxFilterPanel__display">
                    <input onClick={this.props.onCheckBoxFilterChange(this.props.data, !this.props.data.selected)}
                           type="checkbox"
                           defaultChecked={this.props.data.selected}
                           ref="selected"
                    />
                    <div className="CheckboxFilterPanel__display">
                        {this.props.data.label}
                    </div>
                </label>
                <div className="CheckboxFilterPanel__display">{this.props.data.count}</div>
            </div>
        )
    }
}

CheckboxFilterPanelItem.propTypes = {
    data: PropTypes.object,
    onCheckBoxFilterChange: PropTypes.func
}