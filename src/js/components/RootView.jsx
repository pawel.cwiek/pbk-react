import React from "react";
import {ICONS} from './Icon.jsx';
import Results from "./Results.jsx";
import ViewSwitcherToggle from './viewSwitcherToggle/ViewSwitcherToggle.jsx';
import ResultStats from "./ResultStats.jsx";
import CheckboxFilterPanel from "./checkboxFilterPanel/CheckboxFilterPanel.jsx";
import SelectedFilters from "./selectedFilters/SelectedFilters.jsx";
import ResetFiltersButton from "./ResetFiltersButton.jsx"
import RadioGroupFilterPanel from "./radioGroupFilterPanel/RadioGroupFilterPanel.jsx"
import Pagination from "./pagination/Pagination.jsx"
import SortingSelector from "./sortingSelector/SortingSelector.jsx"


const sizeItems = [
    {id: 1, selected: true, label: "90x50mm", count: 112},
    {id: 2, selected: false, label: "85x55mm", count: 67}
];

const styleItems = [
    {id: 1, selected: false, label: "All", count: 208},
    {id: 2, selected: false, label: "Abstraction", count: 12},
    {id: 3, selected: true, label: "Geometry", count: 7},
    {id: 4, selected: false, label: "Men", count: 18},
    {id: 5, selected: false, label: "Wemen", count: 28},
    {id: 6, selected: true, label: "Children", count: 3},
    {id: 7, selected: false, label: "Music", count: 36},
    {id: 8, selected: false, label: "Vintage", count: 14},
    {id: 9, selected: false, label: "Flower", count: 11}
];

const industryItems = [
    {id: 1, selected: false, label: "All", count: 840},
    {id: 2, selected: true, label: "Automotive", count: 112},
    {id: 3, selected: false, label: "Fashion", count: 83},
    {id: 4, selected: false, label: "Law", count: 230},
    {id: 5, selected: true, label: "IT", count: 18},
    {id: 6, selected: false, label: "Sport", count: 66},
    {id: 7, selected: true, label: "Art", count: 20},
    {id: 8, selected: false, label: "Other", count: 311},

];

const favoriteItems = [
    {id: 1, selected: false, label: "Only favorites", count: 22}
];

const optionItems = [
    {id: 1, label: "20"},
    {id: 2, label: "40"},
    {id: 3, label: "All"}
]

const customerRatingItems = [
    {id: 1, selected: false, star: 4, count: 83},
    {id: 2, selected: true, star: 3, count: 230},
    {id: 3, selected: false, star: 2, count: 18},
    {id: 4, selected: false, star: 1, count: 66}
];
const resultItems = [
    {
        id: 1,
        title: 'Automobile',
        subtitle: '90x50mm',
        img: 'https://lh3.googleusercontent.com/CnWNSddrSLWtZHAsgF0eREv2ylMrLypCEVLdCvjnsyW6nFic_niaI7srMUDSO7hX8IlhQPApVH0SHZI5A8XC21IY5lpx9Yr1433lhvEN9QKhP4B3KRKWyY_AudgY7DcAnUXV7g=w205-h151-no',
        ratingValue: 1,
        likeCount: 1
    },
    {
        id: 2,
        title: 'Automobile',
        subtitle: '90x50mm',
        img: 'https://lh3.googleusercontent.com/CnWNSddrSLWtZHAsgF0eREv2ylMrLypCEVLdCvjnsyW6nFic_niaI7srMUDSO7hX8IlhQPApVH0SHZI5A8XC21IY5lpx9Yr1433lhvEN9QKhP4B3KRKWyY_AudgY7DcAnUXV7g=w205-h151-no',
        ratingValue: 2,
        likeCount: 12
    },
    {
        id: 3,
        title: 'Automobile',
        subtitle: '90x50mm',
        img: 'https://lh3.googleusercontent.com/CnWNSddrSLWtZHAsgF0eREv2ylMrLypCEVLdCvjnsyW6nFic_niaI7srMUDSO7hX8IlhQPApVH0SHZI5A8XC21IY5lpx9Yr1433lhvEN9QKhP4B3KRKWyY_AudgY7DcAnUXV7g=w205-h151-no',
        ratingValue: 3,
        likeCount: 34
    },
    {
        id: 4,
        title: 'Automobile',
        subtitle: '90x50mm',
        img: 'https://lh3.googleusercontent.com/CnWNSddrSLWtZHAsgF0eREv2ylMrLypCEVLdCvjnsyW6nFic_niaI7srMUDSO7hX8IlhQPApVH0SHZI5A8XC21IY5lpx9Yr1433lhvEN9QKhP4B3KRKWyY_AudgY7DcAnUXV7g=w205-h151-no',
        ratingValue: 4,
        likeCount: 0
    },
    {
        id: 5,
        title: 'Automobile',
        subtitle: '90x50mm',
        img: 'https://lh3.googleusercontent.com/CnWNSddrSLWtZHAsgF0eREv2ylMrLypCEVLdCvjnsyW6nFic_niaI7srMUDSO7hX8IlhQPApVH0SHZI5A8XC21IY5lpx9Yr1433lhvEN9QKhP4B3KRKWyY_AudgY7DcAnUXV7g=w205-h151-no',
        ratingValue: 5,
        likeCount: 24
    },
    {
        id: 6,
        title: 'Automobile',
        subtitle: '90x50mm',
        img: 'https://lh3.googleusercontent.com/CnWNSddrSLWtZHAsgF0eREv2ylMrLypCEVLdCvjnsyW6nFic_niaI7srMUDSO7hX8IlhQPApVH0SHZI5A8XC21IY5lpx9Yr1433lhvEN9QKhP4B3KRKWyY_AudgY7DcAnUXV7g=w205-h151-no',
        ratingValue: 0,
        likeCount: 123
    },
    {
        id: 7,
        title: 'Automobile',
        subtitle: '90x50mm',
        img: 'https://lh3.googleusercontent.com/CnWNSddrSLWtZHAsgF0eREv2ylMrLypCEVLdCvjnsyW6nFic_niaI7srMUDSO7hX8IlhQPApVH0SHZI5A8XC21IY5lpx9Yr1433lhvEN9QKhP4B3KRKWyY_AudgY7DcAnUXV7g=w205-h151-no',
        ratingValue: 1,
        likeCount: 34
    },
    {
        id: 8,
        title: 'Automobile',
        subtitle: '90x50mm',
        img: 'https://lh3.googleusercontent.com/CnWNSddrSLWtZHAsgF0eREv2ylMrLypCEVLdCvjnsyW6nFic_niaI7srMUDSO7hX8IlhQPApVH0SHZI5A8XC21IY5lpx9Yr1433lhvEN9QKhP4B3KRKWyY_AudgY7DcAnUXV7g=w205-h151-no',
        ratingValue: 2,
        likeCount: 27
    },
    {
        id: 9,
        title: 'Automobile',
        subtitle: '90x50mm',
        img: 'https://lh3.googleusercontent.com/CnWNSddrSLWtZHAsgF0eREv2ylMrLypCEVLdCvjnsyW6nFic_niaI7srMUDSO7hX8IlhQPApVH0SHZI5A8XC21IY5lpx9Yr1433lhvEN9QKhP4B3KRKWyY_AudgY7DcAnUXV7g=w205-h151-no',
        ratingValue: 3,
        likeCount: 3
    },
    {
        id: 10,
        title: 'Automobile',
        subtitle: '90x50mm',
        img: 'https://lh3.googleusercontent.com/CnWNSddrSLWtZHAsgF0eREv2ylMrLypCEVLdCvjnsyW6nFic_niaI7srMUDSO7hX8IlhQPApVH0SHZI5A8XC21IY5lpx9Yr1433lhvEN9QKhP4B3KRKWyY_AudgY7DcAnUXV7g=w205-h151-no',
        ratingValue: 4,
        likeCount: 2
    },
    {
        id: 11,
        title: 'Automobile',
        subtitle: '90x50mm',
        img: 'https://lh3.googleusercontent.com/CnWNSddrSLWtZHAsgF0eREv2ylMrLypCEVLdCvjnsyW6nFic_niaI7srMUDSO7hX8IlhQPApVH0SHZI5A8XC21IY5lpx9Yr1433lhvEN9QKhP4B3KRKWyY_AudgY7DcAnUXV7g=w205-h151-no',
        ratingValue: 5,
        likeCount: 65
    }
]

export default class RootView extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            products: resultItems,
            pageSize: 30,
            total: 120,
            page: 1,
            selectedPageSize: 0,
            filters: [
                {id: 1, type: 'Size', name: '90x50mm'},
                {id: 2, type: 'Orientation', name: 'horizontal'},
                {id: 3, type: 'Industry', name: 'Automotive'},
                {id: 4, type: 'Industry', name: 'Sport'},
                {id: 5, type: 'Industry', name: 'Art'},
                {id: 6, type: 'Style', name: 'Music'}
            ]
        }
    }

    componentDidMount() {
        this.findProducts();
    }

    findProducts() {
        //TODO[pobierz produkty i zaktualizuj states]
    }

    onPageHandler(page) {
        this.setState({page: page}, this.findProducts);
        console.log("[RootView] nextPageHandler() state.page: " + page);
    }

    nextPageHandler() {
        let p = this.state.page + 1;
        this.setState({page: p}, this.findProducts);
        console.log("[RootView] nextPageHandler() state.page: " + p);
    }

    previousPageHandler() {
        let p = this.state.page - 1;
        this.setState({page: p}, this.findProducts);
        console.log("[RootView] previousPageHandler() state.page: " + p);
    }

    removeFilterHandler(item) {
        var array = this.state.filters;
        var index = array.indexOf(item)
        array.splice(index, 1);
        this.setState({filters: array});
    }

    onCheckBoxFilterChangeHandler(filterItem, status) {
        //TODO[uzupełnić states]
        // console.log("[RootView] filterItem: " + filterItem.id + " ,status: " + status);
    }

    onCheckViewSwitchChangeHandler(pageSize) {
        this.setState({selectedPageSize: pageSize});
    }

    render() {
        return (
            <div className="pbk-layout">
                <div className="pbk-layout__body">

                    <div className="pbk-layout__filters">
                        <CheckboxFilterPanel label="Size" data={sizeItems}
                                             onCheckBoxFilterChange={this.onCheckBoxFilterChangeHandler.bind(this)}/>
                        <RadioGroupFilterPanel label="Customer Rating" subtitle="At last" data={customerRatingItems}/>
                        <CheckboxFilterPanel label="Favorites"
                                             onCheckBoxFilterChange={this.onCheckBoxFilterChangeHandler.bind(this)}
                                             data={favoriteItems}/>
                        <CheckboxFilterPanel label="Industry"
                                             onCheckBoxFilterChange={this.onCheckBoxFilterChangeHandler.bind(this)}
                                             data={industryItems}/>
                        <CheckboxFilterPanel label="Style"
                                             onCheckBoxFilterChange={this.onCheckBoxFilterChangeHandler.bind(this)}
                                             data={styleItems}/>
                        <ResetFiltersButton/>
                    </div>
                    <div className="LayoutResults pbk-layout__results pbk-results-list">
                        <div className="ActionBar sk-results-list__action-bar pbk-action-bar">
                            <div className="pbk-actionBarRow">
                                <ResultStats label="Search results:" value={72}/>
                                <SortingSelector options={optionItems}
                                                 labelField='label'
                                                 valueField='id'/>
                                <ViewSwitcherToggle
                                    selected={this.state.selectedPageSize}
                                    onCheckViewSwitchChange={this.onCheckViewSwitchChangeHandler.bind(this)}
                                    items={ [{icon: ICONS.GRID_SMALL, pageSize: 30}, {
                                        icon: ICONS.GRID_MEDIUM,
                                        pageSize: 30
                                    }, {icon: ICONS.GRID_LARGE, pageSize: 30}] }/>
                            </div>
                            <div className="pbk-actionBarRow">
                                <SelectedFilters data={this.state.filters}
                                                 onRemoveFilterClick={this.removeFilterHandler.bind(this)}/>
                            </div>
                        </div>
                        <Results data={this.state.products}/>
                        <Pagination page={this.state.page} pageSize={this.state.pageSize} total={this.state.total}
                                    onPreviousPageLinkClick={this.previousPageHandler.bind(this)}
                                    onNextPageLinkClick={this.nextPageHandler.bind(this)}
                                    onPageLinkClick={this.onPageHandler.bind(this)}/>
                    </div>
                </div>
            </div>
        );
    }
}
