import React, {Component, PropTypes} from 'react'
import Radio from './Radio.jsx'

// TODO[dodać obsługe zamykania]
// TODO[dodać obsługę zapisywania filtru]
export default class RadioGroupFilterPanel extends Component {
    render() {
        return (
            <div className="RadioGroupFilterPanel">
                <div className="RadioGroupFilterPanel__title CheckboxFilterPanel__display">{this.props.label}</div>
                <div className="RadioGroupFilterPanel__panel-close CheckboxFilterPanel__display">x</div>
                <p className="RadioGroupFilterPanel__subtitle">At last</p>
                {
                    this.props.data.map(item =>
                        <Radio key={item.id} data={item}/>)
                }
            </div>
        )
    }
}


RadioGroupFilterPanel.propTypes = {
    label: PropTypes.string,
    subtitle: PropTypes.string,
    data: PropTypes.array,
    onRadioGroupFilterChange: PropTypes.func
}

RadioGroupFilterPanel.defaultProps = {
    label: 'Title',
    subtitle: 'Subtitle',
    data: []
}