import React, {Component, PropTypes} from 'react'
import Rating from '../Rating.jsx'

export default class Radio extends Component {
    render() {
        return (
            <div>
                <div className="RadioGroupFilterPanel__display">
                    <label>
                        <input
                            type="radio"
                            name="group"/>
                        <div className="RadioGroupFilterPanel__display">
                            <Rating valueToShow={this.props.data.star} edit={false}/>
                        </div>
                    </label>
                </div>

                <div className="RadioGroupFilterPanel__display">{this.props.data.count}</div>
            </div>
        );
    }
}