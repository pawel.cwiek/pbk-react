import React, {Component, PropTypes} from 'react'
import Rating from './Rating.jsx'
import LikeButton from './LikeButton.jsx'

export default class Results extends React.Component {

    onChange() {
        console.log('[Results] onChange')
    }

    render() {
        return (
            <div className="Results">
                {this.props.data.map(item =>
                    <div key={item.id} className="Results-item"><img
                        src={item.img}
                        width="205" height="150"/>
                        <div >
                            <div className="Results-item__display-inline">
                                <Rating valueToShow={item.ratingValue} count={5} onChange={this.onChange} size={24} color2={'#ffd700'}/>
                            </div>
                            <div className="Results-item__display-inline">
                                <LikeButton likeCount={item.likeCount}/>
                            </div>
                        </div>
                        <div className="Results-item__title">
                            {item.title}
                        </div>
                        <div className="Results-item__subtitle">{item.subtitle}</div>
                    </div>)}
            </div>
        );
    }
}

Results.propTypes = {
    data: PropTypes.array
}

Results.defaultProps = {
    data: []
}
