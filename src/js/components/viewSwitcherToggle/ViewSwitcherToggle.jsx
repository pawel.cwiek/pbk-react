import React, {Component, PropTypes} from 'react'
import Icon from '../Icon.jsx';

export default class ViewSwitcherToggle extends React.Component {

    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         selected: 0
    //     }
    // }

    onClick(event) {
        // this.setState({selected: event});
        this.props.onCheckViewSwitchChange(event);
    }

    render() {
        var self = this;
        return (
            <div className="pbk-toggle">
                <ul>{
                    self.props.items.map(function (item, index) {
                        var style = '';
                        if (self.props.selected == index) {
                            style = 'selected';
                        }
                        return <li key={index} value={index} className={style} onClick={self.onClick.bind(self, index)}>
                            <Icon
                                icon={item.icon} size="31"/></li>;
                    }) }
                </ul>
            </div>
        );
    }
}

ViewSwitcherToggle.propTypes = {
    onCheckViewSwitchChange: PropTypes.func,
    selected: PropTypes.number
}
ViewSwitcherToggle.defaultProps = {
    selected: PropTypes.number = 0
}
