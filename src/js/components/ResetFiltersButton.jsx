import React, {Component, PropTypes} from 'react'

export default class ResetFiltersButton extends Component {

    render() {
        return (
            <div className="ResetFiltersButton" onClick={this.props.onChange}>
                <div className="ResetFiltersButton__close">x</div>
                <span>Clear all filters</span>
            </div>
        )
    }
}

ResetFiltersButton.propTypes = {
    onChange:   React.PropTypes.func
}