import React, {Component, PropTypes} from 'react'
export default class SelectedFiltersItem extends React.Component {

    onRemoveFilterItemClickHandler() {
        this.props.onRemoveFilterClick(this.props.filter);
    }

    render() {
        return (
            <div
                 className="SelectedFilters-item">
                <div className="SelectedFilters-item__title">{this.props.filter.name}</div>
                <div className="SelectedFilters-item__close" onClick={this.onRemoveFilterItemClickHandler.bind(this)}>x</div>
            </div>
        )
    }
}

SelectedFiltersItem.propTypes = {
    filter: PropTypes.object,
    onRemoveFilterClick: PropTypes.func
}