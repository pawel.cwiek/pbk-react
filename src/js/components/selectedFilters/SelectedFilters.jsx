import React, {Component, PropTypes} from 'react'
import SelectedFiltersItem from './SelectedFiltersItem.jsx'

export default class SelectedFilters extends React.Component {
    render() {
        return (
            <div className="SelectedFilters">
                <div className="SelectedFilters-title">Showing Filters:</div>
                {
                    this.props.data.map(filter =>
                        <SelectedFiltersItem key={filter.id} filter={filter}
                                             onRemoveFilterClick={this.props.onRemoveFilterClick}/>
                    )
                }
            </div>)
    }
}

SelectedFilters.propTypes = {
    data: PropTypes.array,
    onRemoveFilterClick: PropTypes.func
}

SelectedFilters.defaultProps = {
    data: []
}