import React from 'react';
import ReactDOM from 'react-dom';

import RootView from "./components/RootView.jsx";
import Rating from "./components/Rating.jsx";


ReactDOM.render(
	<RootView />,
	document.getElementById('app')
);
